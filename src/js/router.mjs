import { createRouter, createWebHistory } from 'vue-router'
import HomePage from '../views/HomePage.vue'
import ChikaraPage from '../views/ChikaraPage.vue'
import NotFoundComponent from '../views/NotFound.vue'
import NobleFates from '../views/NobleFates.vue'
import Terminal from '../views/Terminal.vue'
import Sandbox from '../views/Sandbox.vue'
import Hideout from '../views/Hideout.vue'
import Testing from '../views/Testing.vue'

// Cloud
import FileUpload from '../cloud/FileUpload.vue'
import FileLogin from '../cloud/FileLogin.vue'
import FileRegister from '../cloud/FileRegister.vue'
import FilesAccount from '../cloud/FileAccount.vue'
import FileManagement from '../cloud/FileManagement.vue'
import Auth0 from '../cloud/Auth0.vue'

// Games
import Days from '../games/Days.vue'
import ASA from '../games/ASA.vue'

// to generate a separate chunk ([name].[hash].js) for a route which is lazy-loaded
// component: () => import('../views/ChikaraView.vue')

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  mode: 'history',
  scrollBehavior: () => ({ top: 0 }),
  linkActiveClass: 'active',
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomePage
    },
    {
      path: '/fileupload',
      name: 'fileupload',
      component: FileUpload
    },
    {
      path: '/login',
      name: 'login',
      component: FileLogin
    },
    {
      path: '/login/auth',
      name: 'auth',
      component: Auth0,
      beforeEnter: async (to, from, next) => {
        const inviteKey = to.query.invite

        if (inviteKey) {
          try {
            const response = await fetch(`/login/auth?invite=${inviteKey}`)

            if (!response.ok) {
              const errorMessage = await response.json()
              console.error(errorMessage.message)
              return next({ component: NotFoundComponent })
            }

            const data = await response.json()
            if (data.authenticated) {
              return next({ component: Auth0 })
            } else {
              return next({ component: NotFoundComponent })
            }
          } catch (err) {
            console.error('An error occurred while checking invite key:', err)
            return next({ component: NotFoundComponent })
          }
        } else {
          return next({ component: NotFoundComponent })
        }
      }
    },
    {
      path: '/register',
      name: 'register',
      component: FileRegister
    },
    {
      path: '/account',
      name: 'account',
      component: FilesAccount
    },
    {
      path: '/filemanagement',
      name: 'filemanagement',
      component: FileManagement
    },
    {
      path: '/chikara',
      name: 'chikara',
      component: ChikaraPage
    },
    {
      path: '/noblefates',
      name: 'noblefates',
      component: NobleFates
    },
    {
      path: '/7days',
      name: '7days',
      component: Days
    },
    {
      path: '/asa',
      name: 'asa',
      component: ASA
    },
    {
      path: '/terminal',
      name: 'terminal',
      component: Terminal
    },
    {
      path: '/sandbox',
      name: 'sandbox',
      component: Sandbox
    },
    {
      path: '/hideout',
      name: 'hideout',
      component: Hideout,
      beforeEnter: async (to, from, next) => {
        try {
          const response = await fetch('/web/checkauth')
          if (response.status === 401) {
            const errorMessage = await response.json()
            console.error(errorMessage.message)
            return next('/')
          } else if (!response.ok) {
            const errorMessage = await response.json()
            console.error('Server error:', errorMessage.message)
            return next('/')
          }

          return next()
        } catch (error) {
          console.error('Fetch error:', error)
          return next('/')
        }
      }
    },
    {
      path: '/testing',
      name: 'testing',
      component: Testing
    },
    {
      path: '/:catchAll(.*)',
      name: 'NotFound',
      component: NotFoundComponent
    }
  ]
})

export default router
