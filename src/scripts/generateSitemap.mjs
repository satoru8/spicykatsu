import { SitemapStream, streamToPromise } from 'sitemap'
import { Readable } from 'stream'
import fs from 'fs'
import path from 'path'

const newDate = new Date().toISOString()

const links = [
  { url: '/', changefreq: 'monthly', priority: 0.8, lastmod: newDate },
  { url: '/fileupload', changefreq: 'monthly', priority: 0.7, lastmod: newDate },
  { url: '/login', changefreq: 'yearly', priority: 0.6, lastmod: newDate },
  { url: '/register', changefreq: 'yearly', priority: 0.6, lastmod: newDate },
  { url: '/account', changefreq: 'monthly', priority: 0.7, lastmod: newDate },
  { url: '/filemanagement', changefreq: 'monthly', priority: 0.7, lastmod: newDate },
  { url: '/chikara', changefreq: 'monthly', priority: 0.7, lastmod: newDate },
  { url: '/noblefates', changefreq: 'yearly', priority: 0.6, lastmod: newDate },
  { url: '/7days', changefreq: 'monthly', priority: 0.7, lastmod: newDate },
  { url: '/asa', changefreq: 'monthly', priority: 0.7, lastmod: newDate },
  { url: '/terminal', changefreq: 'monthly', priority: 0.7, lastmod: newDate },
  { url: '/testing', changefreq: 'monthly', priority: 0.7, lastmod: newDate },
  { url: '/sandbox', changefreq: 'monthly', priority: 0.7, lastmod: newDate }
]

const stream = new SitemapStream({ hostname: 'https://spicykatsu.dev' })

async function generateSitemap() {
  try {
    const data = await streamToPromise(Readable.from(links).pipe(stream))
    const sitemapPath = path.join('C:/nginx/spicykatsu', 'sitemap.xml')
    fs.writeFileSync(sitemapPath, data.toString())
    console.log('Sitemap generated successfully at:', sitemapPath)
  } catch (error) {
    console.error('Error generating sitemap:', error)
  }
}

generateSitemap()
