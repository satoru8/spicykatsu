export default function hero(name) {
  const superheroes = [
    { name: 'Batman', trait: 'You are a night owl with a penchant for dramatic entrances' },
    { name: 'Spider-Man', trait: 'You are unusually fond of hanging around in webbed corners' },
    { name: 'Iron Man', trait: 'You have a thing for iron suits and snarky comments' },
    { name: 'Wonder Woman', trait: 'You believe in truth, justice, and the power of a good lasso' },
    { name: 'Thor', trait: 'You think hammers are suitable for all problems' },
    { name: 'Hulk', trait: 'You have a complicated relationship with anger management' },
    { name: 'Deadpool', trait: "You can't stop breaking the fourth wall" },
    { name: 'Superman', trait: "You can able to rock a cape like nobody's business" },
    { name: 'Captain America', trait: 'Youre an expert shield thrower and barbecue enthusiast' },
    { name: 'Black Widow', trait: 'You are a master of disguise and lover of spy gadgets' },
    { name: 'Green Lantern', trait: 'Always rocking the green ring with some serious cosmic flair' },
    { name: 'Black Panther', trait: 'Youre a king of Wakanda and expert catnapper' },
    { name: 'Doctor Strange', trait: 'master of the mystic arts and connoisseur of bizarre snacks' },
    { name: 'Harley Quinn', trait: 'You have a mad love for chaos and a killer sense of style' },
    { name: 'Supergirl', trait: 'You love flying high with a cape and a smile' },
    { name: 'Shazam', trait: 'Youre always bringing the thunder with a side of magic' },
    { name: 'Ant-Man', trait: 'You like shrinking down for big adventures and tiny snacks' },
    { name: 'Catwoman', trait: 'You are a master thief with a soft spot for felines' },
    { name: 'Hawkeye', trait: 'You are a sharp, skilled marksman and avid fan of purple' }
  ]

  const hash = name.split('').reduce((acc, char) => acc + char.charCodeAt(0), 0)
  const heroIndex = hash % superheroes.length
  const chosenHero = superheroes[heroIndex]
  const output = `${name}, you are ${chosenHero.name}! ${chosenHero.trait}.`

  return output
}
