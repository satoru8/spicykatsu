import chalk from 'chalk'

const getColoredText = (text, colors) =>
  text
    .split('')
    .map((char, index) => chalk.hex(colors[index])(char))
    .join('')

const spicyText = getColoredText('SpicyKatsu', [
  '#ff6b6b',
  '#ffcc5c',
  '#88d8b0',
  '#6a0572',
  '#c84b31',
  '#5a9bd5',
  '#ff9a00',
  '#3a86ff',
  '#d5006d',
  '#f3f3f3'
])

const owl = `
  ${chalk.bgBlack.magenta('___')}                  ${chalk.bgBlack.magenta('___')}
 ${chalk.bgBlack.blue('(o o)')}                ${chalk.bgBlack.blue('(o o)')}
${chalk.bgBlack.green('(  V  )')}  ${spicyText}  ${chalk.bgBlack.green('(  V  )')}
${chalk.bgBlack.red('--m-m------------------m-m--')}
`

console.log(owl)
