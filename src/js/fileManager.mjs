import axios from 'axios'
import { ref } from 'vue'
import router from './router'

const isLoading = ref(false)
const fetchError = ref(null)
const fileList = ref([])
const totalFiles = ref(0)
const isLoggedIn = ref(false)
const user = ref({ username: '' })

export const navTo = (url) => {
  window.location.href = url
}

export function routeTo(url) {
  if (router) {
    router.push(url)
  } else {
    console.error('Router instance is not available.')
  }
}

export const logoutBtn = async (showAlert) => {
  try {
    await axios.post('/web/logout', {
      withCredentials: true
    })
    isLoggedIn.value = false
    localStorage.setItem('isLoggedIn', 'false')
    showAlert('Logged out successfully', 'success')
    routeTo('/login')
  } catch (error) {
    console.error('Failed to log out:', error)
    showAlert('Failed to log out', 'error')
  }
}

export const deleteFile = async (url, fileList, fetchFileList, showConfirm, showAlert) => {
  try {
    const confirmed = await showConfirm('Are you sure you want to delete this file?')
    if (!confirmed) {
      return
    }
    await axios.delete('/web/fm/delete', {
      withCredentials: true,
      data: {
        url
      }
    })
    fileList.value = fileList.value.filter((file) => file.url !== url)
    showAlert('File deleted successfully.', 'success')
  } catch (error) {
    console.error('Failed to delete file:', error)
    showAlert('Failed to delete file.', 'error')
  } finally {
    fetchFileList()
  }
}

export const checkAuthentication = async () => {
  try {
    const response = await axios.get('/web/checkauth', {
      withCredentials: true
    })

    const result = response.data
    if (result.authenticated) {
      isLoggedIn.value = true
      user.value.username = result.username
      localStorage.setItem('isLoggedIn', 'true')
    } else {
      isLoggedIn.value = false
      localStorage.setItem('isLoggedIn', 'false')
      console.log('User not authenticated')
    }
  } catch (error) {
    console.error('Authentication check failed:', error.response ? error.response.data : error.message)
    isLoggedIn.value = false
    localStorage.setItem('isLoggedIn', 'false')
  }
}

export const fetchFileList = async () => {
  isLoading.value = true
  fetchError.value = null
  try {
    const response = await axios.get('/web/files', {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      withCredentials: true
    })

    if (response.status === 401) {
      fetchError.value = 'You are not logged in.'
      isLoggedIn.value = false
      return
    }

    const data = response.data

    if (data && data.files && Array.isArray(data.files)) {
      fileList.value = data.files.map((file) => ({
        name: file.filename,
        url: file.filepath,
        size: file.size
      }))
      totalFiles.value = fileList.value.length
      isLoggedIn.value = true
    } else {
      fetchError.value = 'Unexpected response format'
      throw new Error('Unexpected response format')
    }
  } catch (error) {
    if (error.response && error.response.status === 401) {
      fetchError.value = 'You are not logged in.'
      isLoggedIn.value = false
    } else {
      console.error('Failed to fetch file list:', error)
      fetchError.value = 'Failed to fetch file list'
    }
  } finally {
    isLoading.value = false
  }
}
export { fileList, totalFiles, isLoggedIn, isLoading, fetchError }
