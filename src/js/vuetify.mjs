/**
 * Vuetify global loading & settings
 *
 * @see https://github.com/vuetifyjs/vuetify
 * @see https://vuetifyjs.com/en/features/theme
 */
// import 'vuetify/styles'
import { createVuetify } from 'vuetify'
import { aliases, mdi } from 'vuetify/iconsets/mdi-svg'

import { VNavigationDrawer } from 'vuetify/components/VNavigationDrawer'
import { VList, VListItem } from 'vuetify/components/VList'
import {
  VExpansionPanel,
  VExpansionPanels,
  VExpansionPanelText,
  VExpansionPanelTitle
} from 'vuetify/components/VExpansionPanel'
import { VAppBar, VAppBarNavIcon } from 'vuetify/components/VAppBar'
import { VLayout } from 'vuetify/components/VLayout'
import { VDataTable } from 'vuetify/components/VDataTable'

const vuetify = createVuetify({
  ssr: true,
  autoImport: false,
  icons: {
    defaultSet: 'mdi',
    aliases,
    sets: {
      mdi
    }
  },
  components: {
    VAppBar,
    VAppBarNavIcon,
    VDataTable,
    VExpansionPanel,
    VExpansionPanelText,
    VExpansionPanelTitle,
    VExpansionPanels,
    VLayout,
    VList,
    VListItem,
    VNavigationDrawer
  },
  theme: {
    defaultTheme: 'dark'
    // themes: {
    //   spicyDark: {
    //     dark: true,
    //     colors: {
    //       primary: '#AA00FF',
    //       secondary: '#1dbfff',
    //       accent: '#40C4FF',
    //       error: '#b71c1c',
    //       info: '#2196f3',
    //       success: '#4caf50',
    //       warning: '#ff9800',
    //       text: '#ececec'
    //     }
    //   }
    // }
  }
})

export default vuetify
