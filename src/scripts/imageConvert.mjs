import sharp from 'sharp'
import fs from 'fs/promises'
import path from 'path'

const directoryPath = 'E:/Repo/spicykatsu/dist/assets'

async function convertImagesToWebP(directory) {
  try {
    const files = await fs.readdir(directory)

    const imageFiles = files.filter((file) => /\.(jpg|jpeg|png|webp)$/i.test(file))

    for (const file of imageFiles) {
      const inputPath = path.join(directory, file)
      const outputPath = path.join(directory, path.basename(file, path.extname(file)) + '_New.webp')

      await sharp(inputPath)
        .webp({
          quality: 100,
          alphaQuality: 100,
          effort: 6,
          lossless: false,
          nearLossless: false
        })
        .toFile(outputPath)

      console.log(`${file} converted to ${outputPath}`)
    }
  } catch (err) {
    console.error('Error processing files:', err)
  }
}

convertImagesToWebP(directoryPath)
