import { createApp } from 'vue'
import App from './App.vue'
import router from './js/router'
import vuetify from './js/vuetify'
import { createHead } from '@unhead/vue'
import { SpicyKatsu } from 'spicykatsu'
import { createAuth0 } from '@auth0/auth0-vue'
import 'spicykatsu/dist/style.css'
import './css/main.css'
import './js/greeting'

const app = createApp(App)
const head = createHead()
app.use(head)
app.use(router)
app.use(vuetify)
app.use(SpicyKatsu)

app.use(
  createAuth0({
    domain: import.meta.env.VITE_AUTH_DOMAIN,
    clientId: import.meta.env.VITE_AUTH_CLIENT_ID,
    authorizationParams: {
      redirect_uri: window.location.origin + '/auth/callback'
    }
  })
)

app.mount('#spicy')
