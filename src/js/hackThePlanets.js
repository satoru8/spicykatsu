export default function hackThePlanets() {
  const initializeHack = () => {
    console.log('Initializing planetary hack sequence...')

    const countdownTimer = [3, 2, 1]

    countdownTimer.forEach((countdown, index) => {
      setTimeout(
        () => {
          console.log(countdown)
          if (index === countdownTimer.length - 1) {
            startHack()
          }
        },
        (index + 1) * 1000
      )
    })
  }

  initializeHack()

  const startHack = () => {
    setTimeout(() => {
      const planets = [
        'Earth',
        'Mars',
        'Jupiter',
        'Saturn',
        'Neptune',
        'Uranus',
        'Venus',
        'Mercury',
        'Pluto',
        'Planet Nine',
        'Alpha Centauri',
        'Tatooine',
        'Alderaan'
      ]
      const randomPlanetIndex = Math.floor(Math.random() * planets.length)
      const hackedPlanet = planets[randomPlanetIndex]
      console.log(`Target planet selected: ${hackedPlanet}`)
      executeHack(hackedPlanet)
    }, 2000)
  }

  const executeHack = (hackedPlanet) => {
    setTimeout(() => {
      const messages = [
        `Mission control, we've hacked ${hackedPlanet}!`,
        `Breaking news: ${hackedPlanet} hacked by elite hackers!`,
        `Scientists baffled as ${hackedPlanet} suddenly starts playing Rick Astley's "Never Gonna Give You Up"!`,
        `Aliens on ${hackedPlanet} reported to be jamming to Earth's greatest hits!`,
        `We interrupt this program to bring you a special message from ${hackedPlanet}: "We come in peace, but we love a good prank!"`,
        `Attention all space travelers: ${hackedPlanet}'s new official language is now Emoji! 🚀🌍👽`,
        `Hackers on ${hackedPlanet} spotted wearing sunglasses and drinking cosmic coffee while typing furiously on alien keyboards!`,
        `Scientists discover that ${hackedPlanet}'s orbit has been hacked to form a smiley face! 🙂🪐`,
        `Space pirates commandeer ${hackedPlanet}'s communication satellites to broadcast cat videos throughout the galaxy! 🐱🚀`,
        `Breaking: ${hackedPlanet} invaded by cuddly aliens demanding belly rubs and snacks! 🛸🍪`,
        `Rumors swirl as ${hackedPlanet}'s internet suddenly becomes overrun with memes from the future! #TimeTravelTuesdays`,
        `Scientists detect unusual activity on ${hackedPlanet}: it appears to be throwing a massive intergalactic party! 🎉🌌`,
        `Alert: ${hackedPlanet} has been transformed into a giant disco ball, attracting attention from neighboring star systems! ✨🕺`,
        `Emergency alert: ${hackedPlanet} experiencing a chocolate rainstorm! Residents advised to grab umbrellas and marshmallows! 🍫☔`,
        `Breaking: ${hackedPlanet} declares itself the capital of the universe and issues passports to all sentient beings! 🌌🛂`,
        `Catastrophe: ${hackedPlanet} has been ravaged by alien technology! The galaxy is in danger! 🌎🔫`,
        `Rumors swirl as ${hackedPlanet}'s cybernetic enhancements have been hacked to give you an instant boost! 💪🔥`
      ]
      const randomMessageIndex = Math.floor(Math.random() * messages.length)
      const message = messages[randomMessageIndex]
      console.log(message)
      handleAdditionalActions(hackedPlanet)
    }, 3000)
  }

  const handleAdditionalActions = (hackedPlanet) => {
    setTimeout(() => {
      const additionalActions = {
        Earth: () => {
          console.log(`World leaders on ${hackedPlanet} convene emergency summit to address hack.`)
          console.log(`Social media erupts with #HackThePlanet trending worldwide.`)
        },
        Mars: () => {
          console.log(`Rovers on ${hackedPlanet} mysteriously start posting selfies on Twitter.`)
          console.log(`NASA scientists attempt to communicate with aliens through hacked Mars rovers.`)
        },
        Saturn: () => {
          console.log(`Rings of ${hackedPlanet} reconfigured into interstellar WiFi network.`)
          console.log(`Alien tourists flock to Saturn for selfies with the new cosmic WiFi hotspot.`)
        },
        Neptune: () => {
          console.log(`Hack on ${hackedPlanet} detected. Initiating emergency shutdown.`)
          console.log(`Planetary probes on ${hackedPlanet} perform extensive data analysis.`)
        },
        default: () => {
          console.log(`Strange happenings reported on ${hackedPlanet}, more details to follow.`)
        }
      }
      const additionalAction = additionalActions[hackedPlanet] || additionalActions.default
      additionalAction()
      console.log('Planetary hack sequence completed.')
    }, 2000)
  }
}
